function navbar() {
    if(window.innerWidth <= 480 ) {
        $('.navbar-toggler').show();
        // $('.navbar-collapse').addClass('show');
        $('.navbar-nav').hide();
        $('.navbar-toggler').click( function () {
            if($('.navbar-collapse').hasClass('show')) {
                $('.navbar-nav').hide();
            }else {
                $('.navbar-nav').show();
            }
        });
    }else {
        $('.navbar-nav').show();
        $('.navbar-toggler').hide();
    }
}
$(window).ready(function() {
    navbar();
});
$(window).resize(function() {
    navbar();
});
$(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('.navbar-brand').mouseenter( function () {
            $('.navbar-brand-img').attr('src', 'dist/img/logo-hover@1X.png');
        }).mouseleave( function () {
            $('.navbar-brand-img').attr('src', 'dist/img/Logo@2x.png');
        });
});
