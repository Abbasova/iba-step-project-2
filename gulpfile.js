const
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    babel = require('gulp-babel'),
    uglify = require('gulp-uglify'),
    minifyCSS = require('gulp-clean-css'),
    clean = require('gulp-clean'),
    autoprefixer = require('gulp-autoprefixer'),
    imageMin = require('gulp-imagemin'),
    browserSync = require('browser-sync');

const path = {
    src: {
        scss: 'src/scss/*.scss',
        js: 'src/js/main.js',
        img: 'src/img/**/*'
    },
    dist: {
        self: 'dist/',
        css: 'dist/css/',
        js: 'dist/js/',
        img: 'dist/img/'
    }
};

const cleanDist = () => {
    return gulp.src(path.dist.self, {allowEmpty: true})
    .pipe(clean());
};

const buildSCSS = () => {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', path.src.scss])
    .pipe(sass())
    .pipe(autoprefixer({cascade: false}))
    .pipe(minifyCSS(({compatibility: 'ie7'})))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest(path.dist.css))
};
const buildJS = () => {
    return gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/popper.js/dist/umd/popper.js', 'node_modules/bootstrap/dist/js/bootstrap.js', path.src.js])
    .pipe(concat('script.min.js'))
    // .pipe(babel({
    //     presets: ['@babel/env']
    // }))
    .pipe(uglify())
    .pipe(gulp.dest(path.dist.js));
};

const buildIMG = () => {
    return gulp.src(path.src.img)
    .pipe(imageMin())
        .pipe(gulp.dest(path.dist.img));
};

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    gulp.watch(path.src.scss, buildSCSS).on('change', browserSync.reload);
    gulp.watch(path.src.js, buildJS).on('change', browserSync.reload);
};
gulp.task('build', gulp.series(
    cleanDist,
    buildSCSS,
    buildJS,
    buildIMG,
));
gulp.task('dev', watcher);

